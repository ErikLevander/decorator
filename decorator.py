b = 'Funtion A'


def a():
    print('Function A')


def add_loggin(fn):
    def logged_fn():
        print('Run ' + str(b))
        return fn()

    return logged_fn


a = add_loggin(a)


def add_cleanup(fn):
    def cleaned_fn():
        return_value = fn()
        print('Exit ' + str(b))
        return return_value
    return cleaned_fn


a = add_cleanup(a)

a()
